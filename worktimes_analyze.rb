require 'csv'

INPUT_FILE = '/Users/trainorm/Documents/Tax/my_activity.csv'

def read_activity_file(file)
  CSV.read(file)
end

# Initialize values
last_lock = nil
last_unlock = nil
last_weekday = -1
daily_seconds = 0
last_datetime = nil

my_activity = read_activity_file(INPUT_FILE)

my_activity.each do |a|
  datetime = DateTime.parse(a[0])
  weekday = datetime.wday
  action = a[1]

  # We don't want to process weekends
  next if [0,6].include?(weekday)

  # If a new day:
  #  * output hours worked based on daily seconds counter
  #  * reset daily seconds counter
  if last_weekday != weekday && last_weekday != -1
    hours = ((daily_seconds.to_f / 60.0) / 60.0).truncate(2)
    puts "#{last_datetime.strftime('%a %Y-%m-%d')},#{hours}"
    daily_seconds = 0
  end

  # Handle different events
  if action == 'handleUnlockResult:]_block_invoke'
    last_unlock = datetime
  elsif action == 'sendScreenLockedNotification]'
    last_lock = datetime

    # Calculate diff from last unlock (session time)
    diff = ((last_lock - last_unlock) * 24 * 60 * 60).to_i

    # Add to daily seconds counter
    daily_seconds = daily_seconds + diff
  end

  # Increment values for comparison
  last_weekday = weekday
  last_datetime = datetime
end
