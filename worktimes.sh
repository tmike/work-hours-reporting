#!/bin/bash

OUTPUT_FILENAME="/Users/trainorm/Documents/Tax/my_activity.csv"
LOG_FILENAME="/Users/trainorm/Documents/Tax/my_activity.log"
PREDICATE_STRING='(subsystem == "com.apple.loginwindow.logging")'
EVENTS_PATTERN='loginwindow sending screen is locked notification$|A sending unlock success$'
TIME_PATTERN='^\d+-\d+-\d+\s(0[6-9]|1[0-8])'

function write_log {
    local now=$(date +"%Y-%m-%d %H:%M:%S")
    local msg=$1
    echo "$now - $msg" | tee -a $LOG_FILENAME
}

function last_activity_time {
    local last=$(tail -n 1 $OUTPUT_FILENAME \
	| grep -Eo "^\d+-\d+-\d+\s\d+:\d+:\d+")
    [[ -z "$last" ]] && last="1970-01-01 00:00:00"
    
    last=$(date -j -v +1S -f "%Y-%m-%d %H:%M:%S" "$last" +"%Y-%m-%d %H:%M:%S")

    echo "$last"
}

function record_activity {
    write_log "Determining starting time"
    start=$(last_activity_time)
    write_log "Getting events from start: $start..."
    events=$(log show --start "$start" \
	--predicate "$PREDICATE_STRING" \
	| grep -E "$EVENTS_PATTERN" \
	| awk -F' ' '{print $1,$2","$11}' \
	| grep -E "$TIME_PATTERN")

    events_count=$(echo "$events" | grep -E "^\d+" | wc -l)
    write_log "Found $events_count events"
    if [ $events_count -ge 1 ]
    then
	write_log "Writing events to file: $OUTPUT_FILENAME"
	echo "$events" >> $OUTPUT_FILENAME
    fi
}

record_activity



