# Work Hours Reporting

## Summary

This project is to allow the collection of statistics relating to the time logged into the computer.

  * Script to scrape `log show` output for screen lock/unlock events
  * Script to analyse scraped data and output time worked each day

## Use case

My tax jurisdiction requires exact statistics for time worked for employees that WFH. My company does not use timesheets, so I wanted to gather this information automatically.

## How to use

1. Gather events
   - `worktimes.sh` scrapes the `log show` output periodically via a `cron` job
   - Adds CSV events to output file
   - You will need to setup a cron job to run the script
1. Analyze events
   - `worktimes_analze.rb` reads the events output from `worktimes.sh` script
   - Outputs CSV of daily hours worked based on collected events
